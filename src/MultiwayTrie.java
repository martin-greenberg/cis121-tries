import java.util.Iterator;

/**
 * 26-way Trie Implementation
 * 
 * @author nagornyi
 * @param <E>
 */
public class MultiwayTrie<E> implements TrieI<E> {
	private Node root;
	private int numNodes = 0;
	private int keyCount = 0;
	
	private static class Node{
		private Object val;
		private Node[] next = new Node[26];
		private boolean mapped = false; //is this node representing the end of a value mapping?
		private int numChildren = 0;
	}
	
	/**
	 * Inserts/Updates a key-value pair into the 26-way Trie.
	 * 
	 * @throws IllegalArgumentException
	 *             if index contains characters outside of a-z
	 */
	@Override
	public void put(String index, E item) {
		if((index == null) || (index.length() == 0)){
			throw new IllegalArgumentException();
		}
		for(char c : index.toCharArray()){
			if(!Character.isLowerCase(c)){
				throw new IllegalArgumentException();
			}
		}
		root = put(null, root, index, item, 0);
		keyCount++;
	}
	
	private Node put(Node parent, Node x, String key, E val, int d){
		if(x == null){
			x = new Node();
			numNodes++;
		}
		if (d == key.length()){ 
			x.val = val; 
			x.mapped = true;
			return x;
		}
		int c = key.charAt(d) - 97;
		x.next[c] = put(x, x.next[c], key, val, d+1);
		x.numChildren++;
		return x;
	}

	@Override
	public E get(String index) {
		if((index == null) || (index.length() == 0)){
			throw new IllegalArgumentException();
		}
		for(char c : index.toCharArray()){
			if(!Character.isLowerCase(c)){
				throw new IllegalArgumentException();
			}
		}
		Node x = get(root, index, 0);
		if(x == null){
			return null;
		}
		return (E) x.val;
	}
	
	private Node get(Node x, String key, int d){
		if(x == null){
			return null;
		}
		if(d == key.length()){
			return x;
		}
		int c = key.charAt(d) - 97;
		return get (x.next[c], key, d+1);
	}

	@Override
	public boolean contains(String index) {
		return contains(root, index, 0);
	}
	
	private boolean contains(Node x, String key, int d){
		if(x == null){
			return false;
		}
		if(d == key.length()){
			return x.mapped;
		}
		int c = key.charAt(d) - 97;
		return contains(x.next[c], key, d+1);
	}

	
	/**
	 * Removal support is REQUIRED for MultiwayTries and is not extra credit.
	 */
	@Override
	public void remove(String index) {
		root = remove(root, index, 0);
		this.keyCount--;
	}
	
	private Node remove(Node x, String key, int d){
		if(x == null){
			return null;
		}
		if(d == key.length()){
			x.mapped = false;
		} else {
			int c = key.charAt(d) - 97;
			x.next[c] = remove(x.next[c], key, d+1);
			x.numChildren--;
		}
		if((x.mapped)||(x.numChildren != 0)||(x.equals(root))){
			return x;
		}
		for(int i = 0; i < 26; i++){
			if((x.next[i] != null) && (x.next[i].mapped)){
				return x;
			}
		}
		this.numNodes--;
		return null;
	}

	@Override
	public int size() {
		return this.keyCount;
	}

	@Override
	public int nodeCount() {
		return this.numNodes;
	}

	/**
	 * You do not need to implement iterator for MultiwayTries. No need to
	 * modify this.
	 */
	@Override
	public Iterator<String> iterator() {
		throw new UnsupportedOperationException();
	}

}
