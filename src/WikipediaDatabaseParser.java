import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class parses a Wikipedia XML database file and for each article extracts
 * the article id and the words present in the article text. Since the class
 * implements iterable, it can be the target of a "foreach" statement.
 * 
 * <p>
 * The XML format of the Wikipedia database files is as follows:
 * </p>
 * 
 * <pre>
 * {@code
 * <mediawiki>
 *   <page>
 *     <title>{title}</title>
 *     <ns>{ns}</ns>
 *     <id>{id}</id>
 *     <revision>
 *       <id>{id}</id>
 *       <parentid>{parentid}</parentid>
 *       <timestamp>{timestamp}</timestamp>
 *       <contributor>
 *         <username>{username}</username>
 *         <id>{id}</id>
 *       </contributor>
 *       <minor/>
 *       <comment>{comment}</comment>
 *       <text>{text}</text>
 *       <sha1>{sha1}</sha1>
 *       <model>wikitext</model>
 *       <format>text/x-wiki</format>
 *     </revision>
 *   </page>                                                                                                                                                                   
 *   ...                                                                                                                                                                       
 * </mediawiki>
 * }
 * </pre>
 * 
 * @author nagornyi
 * 
 */
public class WikipediaDatabaseParser implements Iterable<WikipediaDocumentI> {

	private String xmlInputFilename;
	private Collection<WikipediaDocumentI> documents;

	public WikipediaDatabaseParser(String xmlInputFilename) {
		if (xmlInputFilename == null) {
			throw new IllegalArgumentException("Null xml file name.");
		}
		this.xmlInputFilename = xmlInputFilename;
		this.documents = new ArrayList<WikipediaDocumentI>();
	}

	/**
	 * Extracts Wikipedia article ids and text content from a wikipedia database
	 * XML file. Removes all non-lowercase alphabetic characters from article
	 * text content.
	 */
	private void parseWikipediaXml() {
		File wikiXmlFile = new File(this.xmlInputFilename);
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder documentBuilder;
		try {
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(wikiXmlFile);
			NodeList pageList = document.getElementsByTagName("page");

			List<String> curDocumentTokens = null;
			WikipediaDocument curWikiDocument = null;

			int numNodes = pageList.getLength();
			for (int i = 0; i < numNodes; i++) {
				// extract current page element
				Element curElement = (Element) pageList.item(i);
				String curElementId = curElement.getElementsByTagName("id")
						.item(0).getTextContent();
				String title = curElement.getElementsByTagName("title").item(0)
						.getTextContent();
				// extract current revision element from page
				NodeList pageContentList = curElement
						.getElementsByTagName("revision");
				// extract current text element from revision
				Element curTextElement = (Element) pageContentList.item(0);
				String pageText = curTextElement.getElementsByTagName("text")
						.item(0).getTextContent();
				// remove all non-alphabetic characters from article text
				pageText = pageText.replaceAll("[^A-Za-z ]", "");
				// convert all characters to lower case
				pageText = pageText.toLowerCase();
				curDocumentTokens = Arrays.asList(pageText.split(" "));
				// create new WikipediaDocumentI instance
				curWikiDocument = new WikipediaDocument(curElementId, title,
						curDocumentTokens);
				this.documents.add(curWikiDocument);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns an iterator for the parsed Wikipedia documents.
	 */
	@Override
	public Iterator<WikipediaDocumentI> iterator() {
		parseWikipediaXml();
		return this.documents.iterator();
	}

	/**
	 * Internal helper class implementing WikipediaDocumentI.
	 */
	private static class WikipediaDocument implements WikipediaDocumentI {

		private String id;
		private String title;
		private List<String> tokens;

		public WikipediaDocument(String id, String title, List<String> tokens) {
			this.id = id;
			this.title = title;
			this.tokens = tokens;
		}

		@Override
		public String getId() {
			return this.id;
		}

		@Override
		public Iterable<String> getTokens() {
			return this.tokens;
		}

		@Override
		public String getTitle() {
			return this.title;
		}

	}

}
