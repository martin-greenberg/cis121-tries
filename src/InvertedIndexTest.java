import static org.junit.Assert.fail;

import java.util.Map;

import org.junit.Test;

public class InvertedIndexTest {

	/**
	 * Sample test for how to write your own tests using the provided
	 * WikipediaDatabaseParser. Make sure you write your own simple tests that
	 * do NOT use WikipediaDatabaseParser before moving on to tests like this
	 * one.
	 */
	@Test
	public void sampleTestWithWikiData() {
		WikipediaDatabaseParser wikipediaParser = new WikipediaDatabaseParser(
				"wiki_data/wiki_small.xml");
		InvertedIndex it = new InvertedIndex(
				new MultiwayTrie<Map<String, Integer>>());
		it.buildFromDb(wikipediaParser);
		fail("Haven't actually tested anything yet.");
	}

}
