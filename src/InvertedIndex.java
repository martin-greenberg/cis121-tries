import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * This class creates an inverted index from parsed Wikipedia documents.
 * 
 * @author cquanze
 */
public class InvertedIndex {

	/**
	 * Constructs an empty inverted index backed by a given TrieI implementation
	 * 
	 * @param trie
	 *            Some implementation of the TrieI interface. The value
	 *            associated with each key is a Map whose key set contains
	 *            document IDs and whose values are frequencies with which a
	 *            word appears in a document with the given document ID.
	 * @throws IllegalArgumentException
	 *             If the given TrieI is null
	 */
	public InvertedIndex(TrieI<Map<String, Integer>> trie) {
		// TODO Implement
	}

	/**
	 * Build the inverted index by iterating through a database 'cursor'.
	 * 
	 * @param cursor
	 *            The database cursor, throw NullPointerException if null.
	 * @throws IllegalArgumentException
	 *             If the given cursor is null
	 */
	public void buildFromDb(Iterable<WikipediaDocumentI> cursor) {
		// TODO Implement
	}

	/**
	 * Returns the term frequency-inverse document frequency of a term with
	 * respect to a document
	 * 
	 * @param word
	 *            The term for which we are calculating. Throws
	 *            NullPointerException if null
	 * @param id
	 *            The ID of the document with respect to which we are
	 *            calculating
	 * @throws NoSuchElementException
	 *             if word does not exist in ANY document
	 * @return a double that indicates the TF/IDF value
	 */
	public double getTfidf(String word, String id) {
		// TODO Implement
		return 0;
	}

	/**
	 * Returns the set of IDs of the documents that contain a given term
	 * 
	 * @param word
	 *            The term to search for,
	 * @throws NullPointerException
	 *             if word is null
	 * @return a set of ID strings, should return an empty set if the term is
	 *         not found in the corpus
	 */
	public Set<String> getDocuments(String word) {
		// TODO Implement
		return null;
	}

	/**
	 * Returns a list of the top-N documents for a certain search term, ordered
	 * by the TF/IDF of that term in each document in descending order.
	 * 
	 * @param word
	 *            The term to search for. Throws NullPointerException if null is
	 *            given.
	 * @param count
	 *            This is N in top-N, i.e., how many documents to return, at
	 *            most. Throws IllegalArgumentException if this parameter is
	 *            negative or zero.
	 * @return a List of document IDs in descending order of TF/IDF values. The
	 *         list should contain no more than "N=count" elements
	 */
	public List<String> getTopDocuments(String word, int count) {
		// TODO Implement
		return null;
	}

	/**
	 * Returns the total number of nodes (not the key set) used by the inverted
	 * index's underlying trie.
	 * 
	 * @return an int whose value is the total number of nodes used by the
	 *         backing trie
	 */
	public int nodeCount() {
		// TODO Implement
		return -1;
	}
}
