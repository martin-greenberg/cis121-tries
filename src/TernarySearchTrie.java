import java.util.Iterator;

/**
 * Ternary Search trie implementation.
 * 
 * @author nagornyi
 *
 * @param <E>
 */
public class TernarySearchTrie<E> implements TrieI<E> {
	private Node root;
	private int numNodes = 0;
	private int keyCount = 0;
	
	private static class Node{
		private Object val;
		private Node left, mid, right;
		char c;
		private boolean mapped = false; //is this node representing the end of a value mapping?
		private int numChildren = 0;
	}
	
	/**
	 * Inserts/Updates a key-value pair into the 26-way Trie.
	 * 
	 * @throws IllegalArgumentException
	 *             if index contains characters outside of a-z
	 */
	@Override
	public void put(String index, E item) {
		if((index == null) || (index.length() == 0)){
			throw new IllegalArgumentException();
		}
		for(char c : index.toCharArray()){
			if(!Character.isLowerCase(c)){
				throw new IllegalArgumentException();
			}
		}
		root = put(root, index, item, 0);
		keyCount++;
	}
	
	private Node put(Node x, String key, E val, int d){
		char c = key.charAt(d);
		if(x == null){
			x = new Node();
			numNodes++;
			x.c = c;
		}
		if(c<x.c){
			x.left = put(x.left, key, val, d);
		} else {
			if(c>x.c){
				x.right = put(x.right, key, val, d);
			} else {
				if (d < key.length() - 1){
					x.mid = put(x.mid, key, val, d+1);
				} else {
					x.val = val;
					x.mapped = true;
				}
			}
		}
		return x;
	}

	@Override
	public E get(String index) {
		if((index == null) || (index.length() == 0)){
			throw new IllegalArgumentException();
		}
		for(char c : index.toCharArray()){
			if(!Character.isLowerCase(c)){
				throw new IllegalArgumentException();
			}
		}
		Node x = get(root, index, 0);
		if(x == null){
			return null;
		}
		return (E) x.val;
	}
	
	private Node get(Node x, String key, int d){
		if(x == null){
			return null;
		}
		char c = key.charAt(d);
		if(c < x.c){
			return get(x.left, key, d);
		}
		if(c>x.c){
			return get(x.right, key, d);
		}
		if(d < key.length() - 1){
			return get(x.mid, key, d+1);
		}
		return x;
	}

	@Override
	public boolean contains(String index) {
		return contains(root, index, 0);
	}
	
	private boolean contains(Node x, String key, int d){
		if(x == null){
			return false;
		}
		char c = key.charAt(d);
		if(c < x.c){
			return contains(x.left, key, d);
		}
		if(c>x.c){
			return contains(x.right, key, d);
		}
		if(d < key.length() - 1){
			return contains(x.mid, key, d+1);
		}
		return x.mapped;
	}

	@Override
	public int size() {
		return this.keyCount;
	}

	@Override
	public int nodeCount() {
		return this.numNodes;
	}
	/**
	 * To be implemented for extra credit only. Remove the exception when
	 * implementing the extra credit.
	 */
	@Override
	public void remove(String index) {
		throw new UnsupportedOperationException();
	}

	/**
	 * To be implemented for extra credit only. Remove the exception when
	 * implementing the extra credit.
	 */
	@Override
	public Iterator<String> iterator() {
		throw new UnsupportedOperationException();
	}
	
}