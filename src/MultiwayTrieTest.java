import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class MultiwayTrieTest {
	private MultiwayTrie<Integer> trie;
	@Before
	public void setUp() throws Exception {
		trie = new MultiwayTrie<Integer>();
	}

	@Test (expected = IllegalArgumentException.class)
	public void testPutNullKey() {
		trie.put(null, 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testPutEmptyKey() {
		trie.put("", 1);
	}

	@Test (expected = IllegalArgumentException.class)
	public void testPutUppercase() {
		trie.put("A", 1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testPutNonAlphabetic() {
		trie.put("!", 1);
	}
	
	@Test 
	public void testPutValid1Char() {
		trie.put("a", 1);
		assertTrue(trie.contains("a"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 2);
	}
	
	@Test 
	public void testPutValidMultipleChars() {
		trie.put("apple", 1);
		assertTrue(trie.contains("apple"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 6);
	}
	
	@Test 
	public void testPutValidMultipleKeysNonPrefix() {
		trie.put("apple", 1);
		trie.put("banana", 2);
		assertTrue(trie.contains("apple"));
		assertTrue(trie.contains("banana"));
		assertEquals(trie.size(), 2);
		assertEquals(trie.nodeCount(), 12);
	}
	
	@Test 
	public void testPutValidMultipleKeysSharedPrefix() {
		trie.put("table", 1);
		trie.put("tabard", 2);
		assertTrue(trie.contains("table"));
		assertTrue(trie.contains("tabard"));
		assertEquals(trie.size(), 2);
		assertEquals(trie.nodeCount(), 9);
	}
	
	@Test 
	public void testPutValidMultipleKeysFullyPrefixed() {
		trie.put("cat", 1);
		trie.put("catalog", 2);
		assertTrue(trie.contains("cat"));
		assertTrue(trie.contains("catalog"));
		assertEquals(trie.size(), 2);
		assertEquals(trie.nodeCount(), 8);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetNullKey() {
		trie.get(null);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetEmptyKey() {
		trie.get("");
	}

	@Test (expected = IllegalArgumentException.class)
	public void testGetUppercase() {
		trie.get("A");
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testGetNonAlphabetic() {
		trie.get("!");
	}
	
	@Test 
	public void testGetValid1Char() {
		trie.put("a", 1);
		assertTrue(trie.get("a").equals(1));
	}
	
	@Test 
	public void testGetValidMultipleChars() {
		trie.put("apple", 1);
		assertTrue(trie.get("apple").equals(1));
	}
	
	@Test 
	public void testGetValidMultipleKeysNonPrefix() {
		trie.put("apple", 1);
		trie.put("banana", 2);
		assertTrue(trie.get("apple").equals(1));
		assertTrue(trie.get("banana").equals(2));
	}
	
	@Test 
	public void testGetValidMultipleKeysSharedPrefix() {
		trie.put("table", 1);
		trie.put("tabard", 2);
		assertTrue(trie.get("table").equals(1));
		assertTrue(trie.get("tabard").equals(2));
	}
	
	@Test 
	public void testGetValidMultipleKeysFullyPrefixed() {
		trie.put("cat", 1);
		trie.put("catalog", 2);
		assertTrue(trie.get("cat").equals(1));
		assertTrue(trie.get("catalog").equals(2));
	}
	
	@Test
	public void testRemoveOneChar(){
		trie.put("a", 1);
		assertTrue(trie.contains("a"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 2);
		trie.remove("a");
		assertFalse(trie.contains("a"));
		assertEquals(trie.size(), 0);
		assertEquals(trie.nodeCount(), 1);
	}
	
	@Test
	public void testRemoveMultipleChars(){
		trie.put("apple", 1);
		assertTrue(trie.contains("apple"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 6);
		trie.remove("apple");
		assertFalse(trie.contains("a"));
		assertEquals(trie.size(), 0);
		assertEquals(trie.nodeCount(), 1);
	}
	
	@Test
	public void testRemoveTwoKeysNonPrefixed(){
		trie.put("apple", 1);
		trie.put("banana", 2);
		assertTrue(trie.contains("apple"));
		assertTrue(trie.contains("banana"));
		assertEquals(trie.size(), 2);
		assertEquals(trie.nodeCount(), 12);
		trie.remove("apple");
		assertFalse(trie.contains("apple"));
		assertTrue(trie.contains("banana"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 7);
		trie.remove("banana");
		assertFalse(trie.contains("banana"));
		assertEquals(trie.size(), 0);
		assertEquals(trie.nodeCount(), 1);
	}
	
	@Test
	public void testRemoveTwoKeysPartialPrefix(){
		trie.put("table", 1);
		trie.put("tabard", 2);
		assertTrue(trie.contains("table"));
		assertTrue(trie.contains("tabard"));
		assertEquals(trie.size(), 2);
		assertEquals(trie.nodeCount(), 9);
		trie.remove("table");
		assertFalse(trie.contains("table"));
		assertTrue(trie.contains("tabard"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 7);
		trie.remove("tabard");
		assertFalse(trie.contains("tabard"));
		assertEquals(trie.size(), 0);
		assertEquals(trie.nodeCount(), 1);
	}
	
	@Test
	public void testRemoveTwoKeysFullPrefix(){
		trie.put("cat", 1);
		trie.put("catalog", 2);
		assertTrue(trie.contains("cat"));
		assertTrue(trie.contains("catalog"));
		assertEquals(trie.size(), 2);
		assertEquals(trie.nodeCount(), 8);
		trie.remove("cat");
		assertFalse(trie.contains("cat"));
		assertTrue(trie.contains("catalog"));
		assertEquals(trie.size(), 1);
		assertEquals(trie.nodeCount(), 8);
		trie.remove("catalog");
		assertFalse(trie.contains("catalog"));
		assertEquals(trie.size(), 0);
		assertEquals(trie.nodeCount(), 1);
	}
}
