/**
 * Interface which represents a parsed Wikipedia document.
 * 
 * @author cquanze, nagornyi
 * 
 */
public interface WikipediaDocumentI {

	/**
	 * Get the ID of a current wikipedia document
	 * 
	 * @return a string that represents the unique Wikipedia article ID of this
	 *         document
	 */
	public String getId();

	/**
	 * Get the title of a current wikipedia document. Your InvertedIndex should
	 * not need to use this. Instead index by the ID.
	 * 
	 * @return The title of this wikipedia document or an empty string if there
	 *         is no title
	 */
	public String getTitle();

	/**
	 * Get all tokens of the current wikipedia document
	 * 
	 * @return an iterable which can be used to iterate over all tokens in of
	 *         this document
	 */
	public Iterable<String> getTokens();
}