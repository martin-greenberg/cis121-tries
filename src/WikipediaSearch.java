import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * You DO NOT need to submit this class. This is a user interface to run the
 * Wikipedia Search when you have completed the code. A tfidf search is
 * different from traditional keyword searches.
 * 
 * 
 * @author cquanze
 * 
 */
public class WikipediaSearch {
	private static final String database = "wiki_data/wiki_largest.xml";
	private static PatriciaTrie<Map<String, Integer>> kwMap = new PatriciaTrie<Map<String, Integer>>();
	private static InvertedIndex index = new InvertedIndex(kwMap);
	private static PatriciaTrie<String> idMap = new PatriciaTrie<String>();
	private static final JTextArea textArea = new JTextArea(20, 40);
	private static final JTextField searchBox = new JTextField(40);

	private static List<String> keywordSearch(String keyword) {
		if (keyword.equals("")) {
			return null;
		}
		Map<String, Integer> wl = kwMap.get(keyword);
		LinkedList<String> keys = new LinkedList<String>();
		for (String key : wl.keySet()) {
			keys.add(key);
			if (keys.size() == 15) {
				return keys;
			}
		}
		return keys;
	};

	private static List<String> search(String keyword) {
		if (keyword.equals("")) {
			return null;
		} else {
			return index.getTopDocuments(keyword.toLowerCase(), 15);
		}

	}

	private static void loadWindow() {
		final JFrame frame = new JFrame("CIS 121 Wikipedia Search Engine");
		BorderLayout b = new BorderLayout();
		frame.setLayout(b);
		final JLabel label = new JLabel("Search:");
		label.setFont(new Font("Arial", Font.PLAIN, 16));

		searchBox.setFont(new Font("Arial", Font.PLAIN, 16));
		searchBox.setText("");

		// Load text area
		textArea.setEditable(false);
		final JScrollPane scrollPane = new JScrollPane(textArea);
		textArea.setText("  Please wait while database is loading ..."
				+ "\n (Database : " + database + ") ");

		// Make things disabled first
		searchBox.setEditable(false);

		// Add the things
		frame.add(label, BorderLayout.PAGE_START);
		frame.add(searchBox, BorderLayout.CENTER);
		frame.add(scrollPane, BorderLayout.PAGE_END);
		// Hook listeners
		searchBox.addKeyListener(new KeyListener() {
			String searchMode = "tfidf";

			@Override
			public void keyPressed(KeyEvent arg0) {
				// Don't care
			}

			@Override
			public void keyReleased(KeyEvent event) {
				// Detect Enter

				if (event.getKeyCode() == KeyEvent.VK_ENTER) {
					List<String> pages = null;
					if (searchMode.equals("tfidf")) {
						pages = search(searchBox.getText());
					} else {
						pages = keywordSearch(searchBox.getText());
					}
					if (pages == null) {
						textArea.setText("(Please enter a search term in the "
								+ "search field and press enter.)");
						searchBox.setText("");
						return;
					}
					String results = "Results for \"" + searchBox.getText()
							+ "\":\n\n";
					for (String page : pages) {
						results += idMap.get(page)
								+ "   (TFIDF:"
								+ (int) (1000 * index.getTfidf(searchBox
										.getText().toLowerCase(), page))
								/ (float) 1000 + ")" + "\n";
					}
					textArea.setText(results + "\n Total:" + pages.size());
					searchBox.setText("");
				}
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// Don't care
			}

		});
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	private static void loadDB() {
		final WikipediaDatabaseParser db = new WikipediaDatabaseParser(database);
		index.buildFromDb(db);
		for (WikipediaDocumentI doc : db) {
			idMap.put(doc.getId(), doc.getTitle());
		}
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				textArea.setText("(Please enter a search term in the "
						+ "search field and press enter.)");
				searchBox.setEditable(true);
			}
		});
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				loadWindow();
			}
		});
		loadDB();
	}
}
