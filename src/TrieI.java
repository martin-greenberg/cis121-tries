	import java.util.Iterator;

/**
 * Trie interface.
 * 
 * @author cquanze
 * 
 * @param <E>
 */
public interface TrieI<E> extends Iterable<String> {
	/**
	 * Inserts / Updates a key in the Trie with the new value. The index must be
	 * a non-empty string. Note that you are required to support null as a valid
	 * value for item.
	 * 
	 * @throws IllegalArgumentException
	 *             key is null or of length zero
	 * @param index
	 *            key to insert/update
	 * @param item
	 *            value to insert/update
	 */
	public void put(String index, E item);

	/**
	 * Gets the value for a certain key in the Trie, should return null if the
	 * key is not found. You should use contains to test existence of keys since
	 * null is also a valid value for a key.
	 * 
	 * @throws IllegalArgumentException
	 *             key is null or of length zero
	 * @param index
	 *            index of the value you are trying to get
	 * @return the value corresponding to index or null if not found
	 */
	public E get(String index);

	/**
	 * Test to see if the Trie contains a certain key.
	 * 
	 * @param index
	 * @return boolean false if the given key is not in the Trie.
	 */
	public boolean contains(String index);

	/**
	 * Remove a key and its corresponding value from the Trie.
	 * 
	 * @throws UnsupportedOperationException
	 *             if the implementation does not support removal
	 * @param index
	 */
	public void remove(String index);
	
	/**
	 * Returns the size of the key set of the Trie. This should be equal to the
	 * number of key-value pairs stored in the Trie.
	 * 
	 * @return size of key set
	 */
	public int size();

	/**
	 * Returns an iterator for the key set of the Trie. The iterator should go
	 * over the key set in lexicographical order. The iterator will not support
	 * removal.
	 * 
	 * @throws UnsupportedOperationException
	 *             if the implementation does not support iterators
	 * @return iterator over key set
	 */
	public Iterator<String> iterator();

	/**
	 * Returns the number of nodes the Trie currently has. Note that not all the
	 * nodes correspond necessarily to stored pairs key-value, therefore this
	 * typically is strictly larger than size().
	 * 
	 * @return number of nodes
	 */
	public int nodeCount();
}