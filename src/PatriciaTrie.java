import java.util.Arrays;
import java.util.Iterator;

/**
 * Patricia trie implementation.
 * 
 * @author cquanze
 * 
 * @param <E>
 */
public class PatriciaTrie<E> implements TrieI<E> {
	private Node root;
	private int numNodes = 0;
	private int keyCount = 0;
	
	private static class Node{
		private Object val;
		private String key;
		private Node[] next = new Node[26];
		private boolean mapped = false; //is this node representing the end of a value mapping?
		private int numChildren = 0;
	}
	
	/**
	 * Inserts/Updates a key-value pair into the 26-way Trie.
	 * 
	 * @throws IllegalArgumentException
	 *             if index contains characters outside of a-z
	 */
	@Override
	public void put(String index, E item) {
		if((index == null) || (index.length() == 0)){
			throw new IllegalArgumentException();
		}
		for(char c : index.toCharArray()){
			if(!Character.isLowerCase(c)){
				throw new IllegalArgumentException();
			}
		}
		root = put(root, index, item, 0);
		keyCount++;
	}
	
	private Node put(Node x, String key, E val, int d){
		if(x == null){ //case 1: x is null
			x = new Node();
			numNodes++;
		} else {
			int c = key.charAt(d) - 97;
			Node oldChild = x.next[c];
			if(oldChild == null){//case 2: x has no children, or they share no prefix with key
				x.next[c] = put(x.next[c], key, val, d+key.length());
				return x;
			} else { //case 3: x has a child that shares a prefix with key
				String newPrefix = sharedPrefix(oldChild, key);
				char[] oldChildKey = oldChild.key.toCharArray();
				char[] newChildKey = key.toCharArray();
				if(newPrefix.equals(key)){ //case 3a: key is a prefix of x.next[c]
					Node newNode = new Node();
					newNode.mapped = true;
					newNode.key = key;
					newNode.val = val;
					newNode.next = new Node[26];
					x.next[c] = newNode;
					oldChild.key = oldChild.key.substring(newPrefix.length());
					numNodes++;
					x.next[c].next[oldChildKey[newPrefix.length()] - 97] = oldChild;
					return x;
				}
				if(newPrefix.equals(oldChild.key)){ //case 3b: x.next[c] is a prefix of key
					int cb = newChildKey[newPrefix.length()]-97; //first nonprefix character
					x.next[c].next[cb] = put(x.next[c].next[cb], key.substring(newPrefix.length()), val, key.length()-newPrefix.length());
					return x;
				}
					//case 3c: key shares a prefix with x.next[c] -- need to add another child to x for the suffix in key
					Node newChild = new Node();
					String suffix = oldChild.key.substring(newPrefix.length());
					x.next[c].next = new Node[26];
					newChild.key = suffix;
					newChild.mapped = oldChild.mapped;
					newChild.val = oldChild.val;
					newChild.next = oldChild.next;
					x.next[c].next[oldChildKey[newPrefix.length()]-97] = newChild;
					x.next[c].key = newPrefix;
					x.next[c].mapped = false;
					x.next[c].val = null;
					numNodes++;
					String keySuffix = key.substring(newPrefix.length());
					x.next[c].next[newChildKey[newPrefix.length()] - 97] = put(x.next[c].next[newChildKey[newPrefix.length()] - 97], keySuffix, val, key.length() - newPrefix.length());
					return x;
				}
			}
		if(d == key.length()){
			x.val = val;
			x.key = key;
			x.mapped = true;
			return x;
		}
		int c = key.charAt(0) - 97;
		x.next[c] = put(x.next[c], key, val, d+key.length());
		return x;
	}
	
	/**
	 * 
	 * @param a
	 * 		the Node to search for a shared prefix with the string s
	 * @param s
	 * 		the String to match with the local key string on Edge a
	 * @return String of matched prefix if applicable, empty String if none
	 */
	private String sharedPrefix(Node a, String s){
		String output = "";
		if((a.key == null) || (s == null) || (a.key == "") || (s == "")){
			return output;
		}
		char[] edgeString = a.key.toCharArray();
		char[] candString = s.toCharArray();
		int minLen = java.lang.Math.min(edgeString.length, candString.length);
		while(minLen != 0){
			if(edgeString[0] == candString[0]){
				String newChar;
				if(edgeString.length <= candString.length){
					newChar = Character.toString(edgeString[0]);
				} else {
					newChar = Character.toString(candString[0]);
				}
				output = output.concat(newChar);
			} else {
				break;
			}
			minLen--;
			edgeString = Arrays.copyOfRange(edgeString, 1, edgeString.length);
			candString = Arrays.copyOfRange(candString, 1, candString.length);
		}
		return output;
	}
	@Override
	public E get(String index) {
		if((index == null) || (index.length() == 0)){
			throw new IllegalArgumentException();
		}
		for(char c : index.toCharArray()){
			if(!Character.isLowerCase(c)){
				throw new IllegalArgumentException();
			}
		}
		Node x = get(root, index);
		if(x == null){
			return null;
		}
		return (E) x.val;
	}
	
	private Node get(Node x, String key){
		if(((x == null) && (x != root)) || (key == null)){
			return null;
		}
		if((x.key != null) && (x.key.equals(key))){
			return x;
		}
		
		int lenKey = (x.key == null) ? 0 : x.key.length();
		int c = key.charAt(lenKey) - 97;
		return get(x.next[c], key.substring(lenKey));
	}

	@Override
	public boolean contains(String index) {
		return contains(root, index);
	}
	
	private boolean contains(Node x, String key){
		if(((x == null) && (x != root)) || (key == null)){
			return false;
		}
		if((x.key != null) && (x.key.equals(key))){
			return true;
		}
		
		int lenKey = (x.key == null) ? 0 : x.key.length();
		int c = key.charAt(lenKey) - 97;
		return contains(x.next[c], key.substring(lenKey));
	}
	@Override
	public void remove(String index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return this.keyCount;
	}

	@Override
	public int nodeCount() {
		return this.numNodes;
	}
	
	/**
	 * To be implemented for extra credit only. Remove the exception when
	 * implementing the extra credit.
	 */
	@Override
	public Iterator<String> iterator() {
		throw new UnsupportedOperationException();
	}

}
